package com.gcp;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
  private static final String TEMPLATE = "Hello, %s!";

  @GetMapping("/greeting")
  public ResponseEntity<String> greeting() {
    return ResponseEntity.ok(String.format(TEMPLATE, "world!")); 
  }
}